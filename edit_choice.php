<?php
/**
 * Created by PhpStorm.
 * User: WOLF
 * Date: 04.08.2016
 * Time: 13:02
 */

include_once ('inc/config.inc.php');
include_once ('inc/init.inc.php');

$class = NEW ProductTranslation();
$mysql = $class->db();

session_start();
if (!isset($_SESSION['user_id'])) { $_SESSION['user_id'] = "4"; }
include(__DIR__ . "/inc/lang/" . $class->getInterfaceLang($_SESSION['user_id']) . '.inc.php');

$_SESSION['category_id'] = "";

$iso = $class->getInterfaceLang($_SESSION['user_id']);
if (isset($_REQUEST["login"])) {
    $class->login($_REQUEST["user"], $_REQUEST["password"]);
}

if (isset($_REQUEST['action'])) {
    if ($_REQUEST['action'] == 'refresh') {
        $_SESSION['category_id'] = $_REQUEST['category_id'];
    }
    if ($_REQUEST['action'] == 'export') {
        $class->exportItems($_REQUEST['cat']);
    }
    if ($_REQUEST['action'] == 'import') {
        $class->importExcel();
    }
} else {
    $_REQUEST['action'] = '';
}
?>
<head xmlns="http://www.w3.org/1999/html">
    <META HTTP-EQUIV="Content-type"  CONTENT="text/html; charset=windows-1252"><META HTTP-EQUIV="Content-Language"  CONTENT="de">
    <meta name="Author" content="Wolf Schmidt, Paaschburg &amp; Wunderlich GmbH, 04.08.2016" >
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all">
    <title>Paaschburg & Wunderlich GmbH - Edit Translation</title>
</head>

<body>
<div align="center">
    <?php if ($_SESSION["is_loggedin"] == "1") { ?>
        <div style="width: 680px; text-align: left;">
            <?php include("switch_language.php"); ?>
            <fieldset>
                <legend><H2><?php echo $TEXT['select_category']; ?></H2></legend>
                <form action="edit_choice.php" method="post" onsubmit="javascript:return routeCall(this);">
                    <?php $categories = $class->getAllCategories(); ?>
                    <select name="article_category" onchange="javascript:window.location.href='edit_choice.php?action=refresh&category_id='+this.value">
                        <option value=""><?php echo $TEXT['please_select']; ?></option>
                        <?php foreach ($categories as $category) { ?>
                            <?php if ($_SESSION['category_id'] == $category['category_id']) { ?>
                                <option selected="selected" value="<?php echo $category['category_id']; ?>"><?php echo $category['name_en']; ?></option>
                            <?php } else { ?>
                                <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name_en']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </form>

                <!-- Kategorie-Export -->
                <?php if (!$_SESSION['category_id'] == '') { ?>
                    <form action="edit_choice.php?action=export" method="post">
                        <input type="hidden" name="cat" value="<?php echo $_SESSION['category_id']; ?>">
                        <input type="submit" value="<?php echo $TEXT['export_all_articles']; ?>">
                    </form>
                <?php } ?>

                <input type="button" value="<?php echo $TEXT['import_articles']; ?>" onClick="window.open('import.php', 'file'); return false">

            </fieldset>
            <?php if (isset($_SESSION['category_id'])) { ?>
                <?php if ($articles = $class->getArticles($_SESSION['category_id'])) { ?>
                    <ul id="product-list" ">
                    <?php foreach ($articles as $article) { ?>
                        <li>
                            <?php echo $article['sku']; ?>
                            <a href="edit_product.php?sku='<?php echo $article['sku']; ?>'&category='<?php echo $_SESSION['category_id']; ?>'&action=select">
                                <?php echo substr($article['name_' . $class->getLangIso(SOURCE_LANG_ID)],0 , 88); ?></a>
                        </li>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
            </ul>
        </div>
    <?php } else { ?>
    <div style="width: 680px; text-align: left;">
        <?php include("switch_language.php"); ?>
        <fieldset>
            <legend><h2>Login</h2></legend>
            <form action="edit_choice.php">
                <label><?php echo $TEXT['name']; ?></label><input type="text" name="user">
                <label><?php echo $TEXT['password']; ?></label><input type="password" name="password">
                <input type="submit" name="login" value="<?php echo $TEXT['login']; ?>">
            </form>
        </fieldset>
        <?php } ?>
    </div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: Wolf
 * Date: 31.08.2016
 * Time: 14:51
 */

include_once ('inc/config.inc.php');
include_once ('inc/init.inc.php');

$class = NEW ProductTranslation();
$mysql = $class->db();

session_start();
?>

<html>
<head>
    <META HTTP-EQUIV="Content-type"  CONTENT="text/html; charset=windows-1252"><META HTTP-EQUIV="Content-Language"  CONTENT="de">
    <meta name="Author" content="Wolf Schmidt, Paaschburg &amp; Wunderlich GmbH, 04.08.2016" >
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all">
    <title>Paaschburg & Wunderlich GmbH - Data Upload</title>
</head>
<body>
<div align="center">
    <?php if ($_SESSION["is_loggedin"] == "1") { ?>
    <div style="width: 680px; text-align: left;">
        <FIELDSET>
            <LEGEND><h2><?php echo $TEXT['file_upload']; ?></h2></LEGEND>
            <FORM ACTION="translate_import_pruef.php" METHOD="POST" enctype="multipart/form-data">
            <P ALIGN="justify"><?php echo $TEXT['file_upload_text1']; ?></P>
            <P ALIGN="justify"><?php echo $TEXT['file_upload_text2']; ?>
            <UL>
            <LI><?php echo $TEXT['file_upload_text3']; ?></LI>
            <LI><?php echo $TEXT['file_upload_text4']; ?></LI>
            <LI><?php echo $TEXT['file_upload_text5']; ?>
            <OL>
            <LI>SKU</LI>
            <LI>Name_en</LI>
            <LI>Name_it</LI>
            <LI>Short_description_en</LI>
            <LI>Short_description_it</LI>
            <LI>Description_en</LI>
            <LI>Description_it</LI>
            </OL>
            </LI>
            </UL>
            </P>
            <P ALIGN="justify"><?php echo $TEXT['file_upload_text5']; ?></P>
            <HR>
            <p><?php echo $TEXT['file_upload_text7']; ?><br>
                <input name="datei" type="file">
                <span style="float:right;"><input type="submit" value="UPLOAD"></span>
                <INPUT TYPE="hidden" NAME="action" VALUE="UPLOAD">
            </p>
            </form>
    </DIV>
</div>
<?php } ?>
</div>
</body>
</html>
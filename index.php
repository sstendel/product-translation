<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 03.08.2016
 * Time: 15:05
 */

include_once ('inc/config.inc.php');
include_once ('inc/init.inc.php');
include_once ('inc/functions.inc.php');


$mysql = $class->db();
session_start();

if (!isset($_SESSION['user_id'])) { $_SESSION['user_id'] = "4"; }
if (isset($_REQUEST['interface_lang'])) {
    $class->saveInterfaceLang($_SESSION['user_id'], $_REQUEST['interface_lang']);
}


if (!isset($_SESSION["is_loggedin"])) { $_SESSION["is_loggedin"] = "0"; }

/**************** Sprachvariablen laden ****************/
include(__DIR__ . "/inc/lang/" . $class->getInterfaceLang($_SESSION['user_id']) . '.inc.php');
if (isset($_REQUEST['source_lang'])) { $_SESSION['source_lang'] = $_REQUEST['source_lang']; }
if (isset($_REQUEST['target_lang'])) { $_SESSION['target_lang'] = $_REQUEST['target_lang']; }

/*****************************************************/


?>
<head>
    <META HTTP-EQUIV="Content-type"  CONTENT="text/html; charset=windows-1252"><META HTTP-EQUIV="Content-Language"  CONTENT="de">
    <meta name="Author" content="Wolf Schmidt, Paaschburg &amp; Wunderlich GmbH, 04.08.2016" >
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all">
    <title>Paaschburg & Wunderlich GmbH - Edit Translation</title>
</head>
<body>
<div align="center">
    <div style="width: 680px; text-align: left;">
        <?php include("switch_language.php"); ?>
        <span>&nbsp;</span>
        <?php if (!$_SESSION['is_loggedin']) { ?>
            <div style="float: right;">
                <input type=button value="<?php echo $TEXT['login']; ?>" onClick="window.location.href='edit_choice.php'"">
            </div>
        <?php } ?>
        </fieldset>
    </div>
</div>
</body>

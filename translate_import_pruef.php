<?php
//Autor: Wolf Schmidt
//Schmidt Medienservice für Paaschburg & Wunderlich GmbH
//31.08.2016
//alle Rechte liegen beim Autor

include_once ('inc/config.inc.php');
include_once ('inc/init.inc.php');

$class = NEW ProductTranslation();
$mysql = $class->db();

session_start();

$upload_folder = "upload/";
$filename = pathinfo($_FILES['datei']['name'], PATHINFO_FILENAME);
$extension = strtolower(pathinfo($_FILES['datei']['name'], PATHINFO_EXTENSION));
$new_path = $upload_folder.$filename.'.'.$extension;
$mimes = array('application/vnd.ms-excel','text/plain','text/csv','text/tsv');

?>
<HTML>
<head xmlns="http://www.w3.org/1999/html">
	<META HTTP-EQUIV="Content-type"  CONTENT="text/html; charset=windows-1252"><META HTTP-EQUIV="Content-Language"  CONTENT="de">
	<meta name="Author" content="Wolf Schmidt, Paaschburg &amp; Wunderlich GmbH, 04.08.2016" >
	<link href="css/style.css" type="text/css" rel="stylesheet" media="all">
	<link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all">
	<title>Paaschburg & Wunderlich GmbH - Proof Translation</title>
</head>
<BODY>
<div align="center">
	<?php if ($_SESSION["is_loggedin"] == "1") { ?>
	<div style="width: 680px; text-align: left;">
		<?php include("switch_language.php"); ?>
		<fieldset>
			<legend><H2><?php echo $TEXT['proof_import']; ?></H2></legend>
			<?php if(in_array($_FILES['datei']['type'],$mimes)) {

				move_uploaded_file($_FILES['datei']['tmp_name'], $new_path);
				chmod($new_path, 0777);

				$datei = fopen($new_path, "r");
				$daten = fgetcsv($datei, 10000);

				if ($daten[0] === "sku;name_en;name_it;short_description_en;short_description_it;description_en;description_it") {
					echo "<h3>".$TEXT['file_okay']."</h3>";

					while ($daten) {
						$daten = fgetcsv($datei, 10000, ";");
						if (!empty($daten[0]) && !empty($daten[2]) && !empty($daten[4])) {
							echo $daten[0] . " | " . $daten[2] . " | " . $daten[4] . " | " . $daten[6] . "<br>";
							$class->writeProduct($daten[0], $daten[2], $daten[4], $daten[6]);
						} else { ?>
							<?php echo $daten[0] ." | " . $daten[2] . " | " . $daten[4] . " | " . $daten[6] . "<br>"; ?>
							<h4><?php echo $TEXT['not_saved']; ?></h4>
							<?php $class->unlockProduct($daten[0], $daten[2], $daten[4], $daten[6]); ?>
						<?php } ?>
					<?php }
					echo "<h3>" . $TEXT['successfull_saved'] . "</h3>"; ?>
					<span style="float:right;"><button onclick="window.close()"><?php echo $TEXT['close']; ?></button></span>
				<?php }
				else { ?>
					<h4><?php echo $TEXT['wrong_format']; ?></h4>
					<a href="import.php"><h5><?php echo $TEXT['back']; ?></a><span style="float:right;"><button onclick="window.close()"><?php echo $TEXT['close']; ?></button></span></h5>
				<?php } ?>
			<?php } else { ?>
				<h4><?php echo $TEXT['wrong_mime']; ?></h4>
				<a href="import.php"><h5><?php echo $TEXT['back']; ?></a><span style="float:right;"><button onclick="window.close()"><?php echo $TEXT['close']; ?></button></span></h5>
			<?php }
			}
			else {
				print "<h4>Access denied</h4></DIV>";
			}
			?>
		</fieldset>
	</div>
</DIV>
</BODY>
</HTML>
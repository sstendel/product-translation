<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 09.08.2016
 * Time: 14:20
 */

include_once ('inc/config.inc.php');
include_once ('inc/init.inc.php');
include_once ('inc/functions.inc.php');

$class = NEW ProductTranslation();
$mysql = $class->db();

//if (isset($_REQUEST["login"])) {
//    $class->login($_REQUEST["user"], $_REQUEST["password"]);
//}
$myTargetName = '';
$myTargetShortDescription = '';
$myTargetDescription = '';
$state = '';

switch($_REQUEST['action'])
{
    case ('save'):
        /** Update the product and get next sku */
        if ($class->writeProduct($_REQUEST['sku'], $_REQUEST['name_trans'], $_REQUEST['short_description_trans'], $_REQUEST['description_trans']) == true) {
            $message = $TEXT['successfull_saved'];
            $state = 'msg-success';
            if ($class->getNextProduct($_REQUEST['category'])) {
                $product = $class->getNextProduct($_REQUEST['category']);
                $mySku = $product['sku'];
                $mySourceName = $product['name_' . $class->getLangIso(SOURCE_LANG_ID)];
                $mySourceShortDescription = $product['short_description_' . $class->getLangIso(SOURCE_LANG_ID)];
                $mySourceDescription = $product['description_' . $class->getLangIso(SOURCE_LANG_ID)];
                $nameLanguage = $product['name_' . $class->getLangIso(TARGET_LANG_ID)];
                $shortDescriptionLanguage = $product['short_description_' . $class->getLangIso(TARGET_LANG_ID)];
                $descriptionLanguage = $product['description_' . $class->getLangIso(TARGET_LANG_ID)];
            } else {
                $message = $TEXT['no_open_products'];
                $state = 'msg-noproducts';
                $mySku = $message;
                $mySourceName = '';
                $mySourceShortDescription = '';
                $mySourceDescription = '';
                $nameLanguage = '';
                $shortDescriptionLanguage = '';
                $descriptionLanguage = '';
            }
        } else {
            $message = $TEXT['save_error'];
            $state = 'msg-error';
        }
        break;

    case ('select'):
        $product = $class->getProductBySku($_REQUEST['sku']);
        $mySku = $product['sku'];
        $mySourceName = $product['name_' . $class->getLangIso(SOURCE_LANG_ID)];
        $mySourceShortDescription = $product['short_description_' . $class->getLangIso(SOURCE_LANG_ID)];
        $mySourceDescription = $product['description_' . $class->getLangIso(SOURCE_LANG_ID)];
        $nameLanguage = $product['name_' . $class->getLangIso(TARGET_LANG_ID)];
        $shortDescriptionLanguage = $product['short_description_' . $class->getLangIso(TARGET_LANG_ID)];
        $descriptionLanguage = $product['description_' . $class->getLangIso(TARGET_LANG_ID)];
        break;
}
?>

<head xmlns="http://www.w3.org/1999/html">
    <META HTTP-EQUIV="Content-type"  CONTENT="text/html; charset=windows-1252"><META HTTP-EQUIV="Content-Language"  CONTENT="de">
    <meta name="Author" content="Wolf Schmidt, Paaschburg &amp; Wunderlich GmbH, 04.08.2016" >
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/font-awesome.min.css" type="text/css" rel="stylesheet" media="all">
    <title>Paaschburg & Wunderlich GmbH - Edit Translation</title>
</head>

<body>
<div align="center">

    <div style="width: 680px; text-align: left;">

        <?php if($_REQUEST['action'] != 'select') { ?>
            <div id="message" class="show message-box <?php echo $state; ?>"><?php echo $message; ?></div>
        <?php } ?>

        <fieldset>
            <legend><H2><?php echo $mySku; ?></H2></legend>
            <form action="edit_product.php" method="post">
                <input type="hidden" name="action" value="save">
                <input type="hidden" name="sku" value="<?php echo $product['sku']; ?>">
                <input type="hidden" name="category" value="<?php echo $product['category_id']; ?>">

                <label>1. <?php echo $TEXT['trans_name']; ?></label>

                <textarea name="name_en" cols="88" class="text-area disabled" style="height: 50px;"><?php echo $mySourceName; ?></textarea>
                <textarea data-validation="length" data-validation-length="min1"
                          name="name_trans" cols="88" class="text-area" style="height: 50px;"
                          placeholder="Please translate to <?php echo $class->getLangName(TARGET_LANG_ID); ?>"><?php echo $nameLanguage; ?></textarea>
                <p></p>

                <label>2. <?php echo $TEXT['trans_short_description']; ?></label>
                <textarea name="short_description_en" cols="88" class="text-area disabled" style="height: 50px;"><?php echo $mySourceShortDescription; ?></textarea>
                <textarea data-validation="length" data-validation-length="min1"
                          name="short_description_trans" cols="88" class="text-area" style="height: 50px;"
                          placeholder="Please translate to <?php echo $class->getLangName(TARGET_LANG_ID); ?>"><?php echo $shortDescriptionLanguage; ?></textarea>
                <p></p>

                <label>3. <?php echo $TEXT['trans_description']; ?></label>
                <textarea name="description_en" cols="88" class="text-area disabled" style="height: 150px;"><?php echo $mySourceDescription; ?></textarea>
                <textarea name="description_trans" cols="88" class="text-area" style="height: 150px;"
                          placeholder="Please translate to <?php echo $class->getLangName(TARGET_LANG_ID); ?>"><?php echo $descriptionLanguage; ?></textarea>
                <p></p>

                <?php if ($state != 'msg-noproducts') { ?>
                    <input type="submit" name="save" value="<?php echo $TEXT['save_and_next']; ?>">&nbsp;
                    <a href="edit_choice.php?category_id=<?php echo $product['category_id']; ?>"><?php echo $TEXT['back_to_cat']; ?></a>
                <?php } else { ?>
                    <button onclick="location.href='edit_choice.php'"><?php echo $TEXT['back_to_cat']; ?></button>
                <?php } ?>

            </form>
        </fieldset>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<script>
    $.validate({
        lang: 'en'
    });

    $('#message:visible').delay(2000).hide('fade');</script>
</body>
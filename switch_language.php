<fieldset>
    <legend><h2><?php echo $TEXT['welcome']; ?></h2></legend>
    <img src="images/Logo_b4b_2015.jpg" width="650">
    <div>
        <form name='interface_lang' action="index.php" method="post" name="mkz" onsubmit="javascript:return routeCall(this);">
            <label><?php echo $TEXT['select_your'] . $TEXT['system_language']; ?></label><br>
            <select name="interface_lang" onchange="javascript:window.location.href='index.php?interface_lang='+this.value">
                <option value="en" selected="selected"><?php echo $TEXT['please_select']; ?></option>
                <option value="en">English</option>
                <option value="it">Italiano</option>
                <option value="de">Deutsch</option>
                <option value="se">Svenska</option>
                <!--                    <option value="fr">Francais</option>-->
            </select>
        </form>
        <hr>
    </div>

    <?php if($_SESSION['is_loggedin']) { ?>
        <div>
            <form name='translate' action="index.php" method="post" name="mkz" onsubmit="javascript:return routeCall(this);">
                <label><?php echo $TEXT['select_your'] . $TEXT['source_language']; ?></label><br>
                <select name="systemspeech" onchange="javascript:window.location.href='index.php?source_lang='+this.value">
                    <option value="en" selected="selected"><?php echo $TEXT['please_select']; ?></option>
                    <option value="en">English</option>
                    <option value="de">Deutsch</option>
                </select>
            </form>
            <hr>
        </div>
        <div>
            <form name='translate' action="index.php" method="post" name="mkz" onsubmit="javascript:return routeCall(this);">
                <label><?php echo $TEXT['select_your'] . $TEXT['target_language']; ?></label><br>
                <select name="systemspeech" onchange="javascript:window.location.href='index.php?target_lang='+this.value">
                    <option value="en" selected="selected"><?php echo $TEXT['please_select']; ?></option>
                    <option value="cz">Czech</option>
                    <option value="en">English</option>
                    <option value="fr">Francaise</option>
                    <option value="it">Italiano</option>
                    <option value="pl">Polsky</option>
                    <option value="se">Svenska</option>
                </select>
            </form>
            <hr>
        </div>
        <div>
            <span class="trans-direction"><?php echo $TEXT['translate_direction']; ?>:</span><img src="images/flags/<?php echo $_SESSION['source_lang']; ?>.png">
            <i class="fa fa-arrow-right" aria-hidden="true" style="position: relative; top: -12px; padding: 0 5px"></i>
            <img src="images/flags/<?php echo $_SESSION['target_lang']; ?>.png">
        </div>
    <?php } ?>

<?php
/**
 * Autor: Schmidt Medienservice, Wolf Schmidt
 * für Paaschburg & Wunderlich
 * 04.08.2016
 */

include_once ('../inc/config.inc.php');
include_once ('../inc/functions.inc.php');

$class = NEW ProductTranslation();
$mysql = $class->db();

ini_set("memory_limit",'512M');
ini_set('max_execution_time', 3600);

define('IMPORTFILE', 'artikel.sdf');

if ($importFile = fopen(IMPORTFILE, 'r')) {
    $sql = $mysql->query("TRUNCATE TABLE products");
} else {
    echo "[" . date('d.m.Y h:i:s') . "] " . "File " . IMPORTFILE . " not in dirctory!\n";
    exit;
}

$count = 0;
while (($line = fgetcsv($importFile)) !== FALSE) {
    $count++;

    $insertFahrzeugQuery = $mysql->query("INSERT INTO products
                            VALUES
                            ('$line[0]' ,
                            '$line[1]' ,
                            '$line[2]' ,
                            '$line[3]' ,
                            '$line[4]' ,
                            '$line[5]' ,
                            '$line[6]' ,
                            '$line[7]' ,
                            '$line[8]' ,
                            '$line[9]' ,
                            '$line[10]' ,
                            '$line[11]' ,
                            '$line[12]' ,
                            '$line[13]' ,
                            '$line[14]' ,
                            '$line[15]' ,
                            '$line[16]' ,
                            '0',
                            '0',
                            '0',
                            '0',
                            '',
                            '',
                            '',
                            '')
                            ");
}
print "Die Datenbanktabelle <B>products</B> wurde aktualisiert.";

fclose($importFile);
<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 03.08.2016
 * Time: 15:07
 */

class ProductTranslation
{

    /**
     * @return mysqli
     */
    public function db()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        $mysqli->set_charset("utf8");
        return $mysqli;
    }

    public function getText($text)
    {

        $mysql = ProductTranslation::db();
        $text_id = $mysql->query("SELECT text_id FROM text_libary WHERE value like '$text'");
        $_textId = $text_id->fetch_assoc();

        $language_text = $mysql->query("SELECT value FROM text_libary WHERE text_id = $_textId[text_id] AND languages_id = $_SESSION[language]");
        $count = $language_text->num_rows;
        if ($count > 0) {
            $text = $language_text->fetch_assoc();
            return $text['value'];
        } else {
            $language_text = $mysql->query("SELECT value FROM text_libary WHERE text_id = $_textId[text_id] AND languages_id = 2");
            $text = $language_text->fetch_assoc();
            return $text['value'];
        }
    }

    /**
     * @return bool|mysqli_result
     */
    public function getAllCategories()
    {
        $mysql = ProductTranslation::db();
        $result = $mysql->query("SELECT cat.* FROM categories cat JOIN products pro ON cat.category_id = pro.category_id WHERE `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = 0 GROUP BY cat.name_de");
        while ($row = $result->fetch_array()) {
            $categories[] = $row;
        }
        return $categories;
    }

    /**
     * @param $category
     * @return bool|mysqli_result|string
     */
    public function getArticles($category)
    {
        $mysql = ProductTranslation::db();
        $articles = $mysql->query("SELECT sku, name_en, `name_" . $this->getLangIso(TARGET_LANG_ID) . "` 
        , short_description_en, `short_description_" . $this->getLangIso(TARGET_LANG_ID) . "`
        , description_en, `description_" . $this->getLangIso(TARGET_LANG_ID) . "`
        FROM " . TABLE_PRODUCTS . " WHERE category_id = " . $category . " AND `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = 0");
        if ($articles) {
            return $articles;
        } else {
            return false;
        }
    }


    public function getNextProduct($category_id)
    {
        $mysql = ProductTranslation::db();
        $result = $mysql->query("SELECT * FROM " . TABLE_PRODUCTS . " WHERE category_id = " . $category_id . " AND `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = 0 ORDER BY sku ASC LIMIT 1");
        return $result->fetch_assoc();
    }


    /**
     * @param $sku
     * @return object|stdClass
     */
    public function getProductBySku($sku)
    {
        $mysql = ProductTranslation::db();
        $result = $mysql->query("SELECT * FROM products WHERE sku = " . $sku . " AND `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = 0");
        return $result->fetch_array();
    }


    /**
     * @param $langId
     * @return mixed
     */
    public function getInterfaceLang($userId)
    {
        $mysql = ProductTranslation::db();
        $result = $mysql->query("SELECT interface_lang FROM user WHERE id = $userId;");
        $row = $result->fetch_array();
        return $row['interface_lang'];
    }

    public function saveInterfaceLang($userId, $iso)
    {
        $mysql = ProductTranslation::db();
        if ($userId == 4 ) {
        $sql = "UPDATE user SET interface_lang='$iso' WHERE id = $userId";
        $mysql->query($sql);
        } else {
            $sql = "UPDATE user SET interface_lang='$iso' WHERE id = $userId";
            $mysql->query($sql);
            $sql2 = "UPDATE user SET interface_lang='en', source_lang='de', target_lang='' WHERE id = 4";
            $mysql->query($sql2);
        }
        if ($mysql->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $langId
     * @return mixed
     */
    public function getLangName($langId)
    {
        $mysql = ProductTranslation::db();
        $result = $mysql->query("SELECT name FROM languages WHERE id = $langId;");
        $row = $result->fetch_array();
        return $row['name'];
    }


    /**
     * @param $user
     * @param $password
     */
    public function login($user, $password)
    {
        $mysql = ProductTranslation::db();
        $result = $mysql->query("SELECT * FROM user WHERE benutzername like '$user' AND passwort like '$password'");
        if ($result) {
            $result->fetch_array();
            $count = $result->num_rows;
            $_SESSION["is_loggedin"] = $count;
        } else {
            $_SESSION["is_loggedin"] = 0;
        }
    }


    /**
     * @param $sku
     * @param $name
     * @param $short_description
     * @param $description
     * @return string
     */
    public function writeProduct($sku, $name, $short_description, $description)
    {
        $mysql = ProductTranslation::db();
        $stmt = "UPDATE " . TABLE_PRODUCTS . " SET 
            `name_" . $this->getLangIso(TARGET_LANG_ID) . "` = '" . $name . "',
            `short_description_" . $this->getLangIso(TARGET_LANG_ID) . "` = '" . $short_description . "',
            `description_" . $this->getLangIso(TARGET_LANG_ID) . "` = '" . $description . "',
            `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = '1',
            `timestamp_" . $this->getLangIso(TARGET_LANG_ID) . "` = NOW() 
            WHERE `sku` = '" . $sku . "';";
        $mysql->query($stmt);
//        echo $stmt . '<br />';
//        echo $mysql->affected_rows . '<br />';
        if ($mysql->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $sku
     */
    public function blockProduct($sku)
    {
        $mysql = ProductTranslation::db();
        $stmt = "UPDATE " . TABLE_PRODUCTS . " SET 
            `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = '2',
            `timestamp_" . $this->getLangIso(TARGET_LANG_ID) . "` = NOW() 
            WHERE `sku` = '" . $sku . "';";
        $mysql->query($stmt);

        if ($mysql->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function unlockProduct($sku, $name, $short_description, $description)
    {
        $mysql = ProductTranslation::db();
        $stmt = "UPDATE " . TABLE_PRODUCTS . " SET 
        `name_" . $this->getLangIso(TARGET_LANG_ID) . "` = '" . $name . "',
            `short_description_" . $this->getLangIso(TARGET_LANG_ID) . "` = '" . $short_description . "',
            `description_" . $this->getLangIso(TARGET_LANG_ID) . "` = '" . $description . "',

            `status_" . $this->getLangIso(TARGET_LANG_ID) . "` = '0',
            `timestamp_" . $this->getLangIso(TARGET_LANG_ID) . "` = NOW() 
            WHERE `sku` = '" . $sku . "';";
        $mysql->query($stmt);

        if ($mysql->affected_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function exportItems($cat) {

        $articles = $this->getArticles($cat);

        function cleanData(&$str)
        {
            $str = preg_replace("/\t/", "\\t", $str);
            $str = preg_replace("/\r?\n/", "\\n", $str);
            if(strstr($str, '"')) $str = '"' . str_replace('"', '""', $str) . '"';

            // Zahlenformat und Datum als String
            if(preg_match("/^0/", $str) || preg_match("/^\+?\d{8,}$/", $str) || preg_match("/^\d{4}.\d{1,2}.\d{1,2}/", $str)) {
                $str = "'$str";
            }
        }

        $filename = "Translate Items_" . date('Ymd') . ".xls";

        header("Content-Disposition: attachment; filename=\"$filename\"");
        header("Content-Type: application/vnd.ms-excel");

        $flag = false;
        define('SEPERATOR','"' );
        foreach($articles as $row) {
            if(!$flag) {
                echo implode("\t", array_keys($row)) . "\r\n";
                $flag = true;
            }

            echo $row["sku"]."\t" .
                SEPERATOR . $row["name_en"] . SEPERATOR . "\t" .
                SEPERATOR . $row["name_".ISO] . SEPERATOR . "\t" .
                SEPERATOR . $row["short_description_en"] . SEPERATOR . "\t" .
                SEPERATOR . $row["short_description_".ISO] . SEPERATOR . "\t" .
                SEPERATOR . $row["description_en"] . SEPERATOR . "\t" .
                SEPERATOR . $row["description_".ISO] . SEPERATOR .
                "\n";

            $this->blockProduct($row['sku']);
        }
        exit;
    }

    public function importExcel($file) {
        echo "Wird Import Funktion";
        exit;
    }
}


<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 12.08.2016
 * Time: 13:13
 */

$TEXT['welcome'] = 'Paaschburg & Wunderlich Übersetzungsdatenbank';
$TEXT['select_your'] = 'Wählen Sie Ihre ';
$TEXT['select_category'] = 'Bitte Kategorie wählen';
$TEXT['please_select'] = 'Bitte auswählen';
$TEXT['system_language'] = 'Systemsprache';
$TEXT['source_language'] = 'Ausgangssprache';
$TEXT['target_language'] = 'Zielsprache';
$TEXT['translate_direction'] = 'Richtung';
$TEXT['back'] = 'Zurück';
$TEXT['close'] = 'Fenster schließen';

$TEXT['back_to_cat'] = 'Zurück zur Kategorieauswahl';
$TEXT['save_and_next'] = 'Speichern und nächste SKU';
$TEXT['login'] = 'Anmelden';
$TEXT['name'] = 'Name: ';
$TEXT['password'] = 'Passwort: ';
$TEXT['export_all_articles'] = 'Export alle Artikel der gewählten Kategorie';
$TEXT['import_articles'] = 'Import bearbeiteter Artikel';
$TEXT['proof_import'] = 'Prüfe Import-File';
$TEXT['file_okay'] = 'Datei korrekt!';
$TEXT['wrong_mime'] = 'Uups, falsches Dateiformat!';
$TEXT['wrong_format'] = 'Die Kopfzeile stimmt nicht mit dem Importlayout überein!';
$TEXT['not_saved'] = 'Artikel nicht aktualisiert';

$TEXT['trans_name'] = 'Name';
$TEXT['trans_short_description'] = 'Kurzbeschreibung';
$TEXT['trans_description'] = 'Beschreibung';

$TEXT['successfull_saved'] = 'Übersetzung gespeichert';
$TEXT['no_open_products'] = 'Keine unbearbeiteten Produkte in dieser Kategorie';
$TEXT['save_error'] = 'Speicherfehler!!!';

$TEXT['file_upload'] = 'Datei Upload';
$TEXT['file_upload_text1'] = 'Hier haben Sie die Möglichkeit per Upload Artikelübersetzungen bequem hochzuladen. 
                                Dazu eben Sie den Pfad zu der hochzuladenen Datei an. Nach bestätigen des UPLOAD Buttons werden die Datensätze geprüft und Ihnen nochmals angezeigt. 
                                Wenn die Daten korrekt importiert sind, bestätigen Sie den Upload.';
$TEXT['file_upload_text2'] = 'Bedingung für den fehlerfreien Import der Dateien ist die richtige Datenkonsistenz. 
                                Um sicherzustellen dass Ihre Dateien den Vorgaben entsprechen überprüfen Sie bitte folgende Punkte:';
$TEXT['file_upload_text3'] = 'Die Datei muss eine Semikolon getrennte CSV-Datei sein.';
$TEXT['file_upload_text4'] = 'Die Datei muss in der ersten Zeile die Feldnamen enthalten.';
$TEXT['file_upload_text5'] = 'Die Datei muss zwingend folgende Felder enthalten:';
$TEXT['file_upload_text6'] = 'Die Felder können leer sein, jedoch müssen sie in Ihrer Datei vorhanden sein. 
                                Wenn Sie hierzu Fragen haben oder sich nicht sicher sind dass Ihre Datei den Vorgaben entspricht wenden Sie sich bitte an Ihren Systemadministrator.';
$TEXT['file_upload_text7'] = 'Wählen Sie eine Textdatei (.csv) von Ihrem Rechner aus:';
<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 12.08.2016
 * Time: 13:13
 */

$TEXT['welcome'] = 'Paaschburg & Wunderlich Översättningsdatabas';
$TEXT['select_your'] = 'Välj din ';
$TEXT['select_category'] = 'Vänligen välj kategori';
$TEXT['please_select'] = 'Välj';
$TEXT['system_language'] = 'Systemspråk';
$TEXT['source_language'] = 'Källspråk';
$TEXT['target_language'] = 'Målspråk';
$TEXT['translate_direction'] = 'Riktning';
$TEXT['back'] = 'Tillbaka';
$TEXT['close'] = 'Stäng Fönstret';

$TEXT['back_to_cat'] = 'Återgå till kategori';
$TEXT['save_and_next'] = 'Spara och nästa SKU';
$TEXT['login'] = 'Registrera';
$TEXT['name'] = 'Namn: ';
$TEXT['password'] = 'Lösenord: ';
$TEXT['export_all_articles'] = 'Exportera alla poster i den valda kategorin';
$TEXT['import_articles'] = 'Import av bearbetade produkter';
$TEXT['proof_import'] = 'Kolla importfil';
$TEXT['file_okay'] = 'Fil korrekt!';
$TEXT['wrong_mime'] = 'Uups, felaktigt filformat!';
$TEXT['wrong_format'] = 'Huvudet stämmer inte överens med import layout!';
$TEXT['not_saved'] = 'Uppdateras inte';

$TEXT['trans_name'] = 'Namn';
$TEXT['trans_short_description'] = 'Sammanfattning';
$TEXT['trans_description'] = 'Beskrivning';

$TEXT['successfull_saved'] = 'Översättning lagras';
$TEXT['no_open_products'] = 'Inga obearbetade produkter i denna kategori';
$TEXT['save_error'] = 'Minne Fel!!!';

$TEXT['file_upload'] = 'Filuppladdning';
$TEXT['file_upload_text1'] = 'Här har du möjlighet att på uppladdnings artiklar Översättningar ladda bekväm.
                                 Dessa ger sökvägen till filen som ska laddas upp. Efter LADDA UPP-knappen för att bekräfta poster kontrolleras och visas igen.
                                 Om data importeras korrekt, bekräftar uppladdningen.';
$TEXT['file_upload_text2'] = 'Villkor för felfri import av filer är rätt data konsistens.
                                 Se till att dina filer uppfyller kraven kontrollera följande punkter:';
$TEXT['file_upload_text3'] = 'Filen måste vara en semikolonseparerad CSV-fil.';
$TEXT['file_upload_text4'] = 'Filen måste innehålla den första raden fältnamnen .';
$TEXT['file_upload_text5'] = 'Filen måste innehålla följande områden:';
$TEXT['file_upload_text6'] = 'Fält kan vara tomt, men de måste vara närvarande i din fil.
                                 Om du har några frågor eller är osäker på om filen motsvarar specifikationerna, kontakta din systemadministratör .';
$TEXT['file_upload_text7'] = 'Välj en textfil (CSV) från datorn:';
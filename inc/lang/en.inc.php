<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 12.08.2016
 * Time: 13:13
 */

$TEXT['welcome'] = 'Paaschburg & Wunderlich Translate Database';
$TEXT['select_your'] = 'Select Your ';
$TEXT['select_category'] = 'Plesae select a category to translate products';
$TEXT['please_select'] = 'Please select';
$TEXT['system_language'] = 'system language';
$TEXT['source_language'] = 'Source Language';
$TEXT['target_language'] = 'Target Language';
$TEXT['translate_direction'] = 'Direction';
$TEXT['back'] = 'Back';
$TEXT['close'] = 'Close window';

$TEXT['back_to_cat'] = 'Back to Category selection';
$TEXT['save_and_next'] = 'Save & next Product';
$TEXT['login'] = 'Login';
$TEXT['name'] = 'Name: ';
$TEXT['password'] = 'Password: ';
$TEXT['export_all_articles'] = 'Export all items of selected category';
$TEXT['import_articles'] = 'Import edited items';
$TEXT['proof_import'] = 'Proof imported file';
$TEXT['file_okay'] = 'File okay!';
$TEXT['wrong_mime'] = 'Sorry, mime type not allowed!';
$TEXT['wrong_format'] = 'Header does not match the import layout!';
$TEXT['not_saved'] = 'Item not updated';

$TEXT['trans_name'] = 'Name';
$TEXT['trans_short_description'] = 'Short Description';
$TEXT['trans_description'] = 'Description';

$TEXT['successfull_saved'] = 'Translation successfully saved';
$TEXT['no_open_products'] = 'No open products for this Ccategory';
$TEXT['save_error'] = 'Error!!!';

$TEXT['file_upload'] = 'File upload';
$TEXT['file_upload_text1'] = 'Here you have the possibility via upload articles Translations upload comfortable. 
                              These provide the path to the file to be uploaded. After the UPLOAD button to confirm the records checked and displayed again. 
                              If the data is imported correctly, confirm the upload.';
$TEXT['file_upload_text2'] = 'Condition for error-free import of files is the correct data consistency. 
                                Please make sure that your files meet the requirements check the following points:';
$TEXT['file_upload_text3'] = 'The file must be a semicolon-delimited CSV file.';
$TEXT['file_upload_text4'] = 'The file must contain the first line the field names.';
$TEXT['file_upload_text5'] = 'The file must contain the following fields:';
$TEXT['file_upload_text6'] = 'Fields may be empty, but they must be present in your file.
                                 If you have any questions or are not sure that your file corresponds to the specifications, please contact your system administrator.';
$TEXT['file_upload_text7'] = 'Select a text file (.csv) from your computer:';